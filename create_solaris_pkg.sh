#!/bin/bash

#
# Solaris packages (pre Sol 11) seem to require no less than three commands to create.
# This script will create a very simple package.
#
# Example:  create_solaris_pkg.sh -n FOObar -d "Good foo software." /usr/local/bin/foobar /usr/local/lib/foobar

usageexit() {
    cat <<EOF
Usage: $0 -n package_name -d "short description" src [ src .... ]

Creates a simple solaris package. The package will install all the 'src' to the
same location when installed. The 'src' can be files or directories.

 -d <"stuff">  descriptive name of package
 -n <name>     package name like FOOxxxx
 <src>         source directory or file. repeat to add more.
EOF

    exit 1
}

#
# Tempfiles.
proto_tmpfile=/var/tmp/pkgproto-$$
proto_tmpfile_body=/var/tmp/pkgproto-2-$$
info_tmpfile=/var/tmp/pkginfo-$$
pkg_destinationdir=/var/tmp   # The package will end up as a file in this directory.

#
# Setup.
while getopts "d:n:" opt;
do
    case $opt in
        d) description=$OPTARG ;;
        n) pkgname=$OPTARG ;;
        \?) usageexit ;;
        *) usageexit ;;
    esac
done

shift $(($OPTIND -1))
pkg_src=$@

if [ -z "$description" ] || [ -z "$pkg_src" ] || [ -z "$pkgname" ] ; then
    usageexit
fi

cat<<EOF
Constructing package $pkgname to directory $pkg_destinationdir
EOF

#
# Make pkginfo with generic version based on date.
cat > $info_tmpfile <<EOF
PKG="$pkgname"
NAME=$description
VERSION=`date +"%y%m%d_%H%M%S"`
ARCH=`uname -i`
CATEGORY="application"
BASEDIR=/
EOF

#
# Make pkgproto which is basically list of files.

# header. preremove/postinstall scripts would also go here.
cat > $proto_tmpfile <<EOF
i pkginfo=$info_tmpfile
EOF

# List of files. Certain characters like spaces cause issues, due to bug in the package commands.
for s in $pkg_src
do
    echo Adding $s to proto file...
    # pkgproto constructs the list of files in correct format.
    # This invocation will make them all get installed to the same place on the destination.
    # To make the files get installed elsewhere on the system, you would use the format $sdir=$dest_dir.
    /bin/pkgproto $s >> $proto_tmpfile_body
done

# Avoid errors on duplicates.
cat $proto_tmpfile_body|sort|uniq >> $proto_tmpfile

#
# Run mkpkg to process the pkgproto file.
echo "run mkpkg"
pkgmk -r / -d $pkg_destinationdir -f $proto_tmpfile -o

#
# Run pkgtran to package it all up.
echo "run pkgtrans"
final_destination=$pkg_destinationdir/$pkgname-`uname -i`.pkg
if pkgtrans -s $pkg_destinationdir $final_destination $pkgname ; then
    echo "Package file created: $final_destination"
else
    echo "Problem creating: $final_destination"
fi

#
# Cleanup.
rm $proto_tmpfile $proto_tmpfile_body $info_tmpfile

